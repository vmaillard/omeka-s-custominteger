<?php
namespace CustomInteger;

return [
    'api_adapters' => [
        'invokables' => [
            'custom_integer' => Api\Adapter\CustomIntegerAdapter::class,
        ],
    ],
    'entity_manager' => [
        'mapping_classes_paths' => [
            dirname(__DIR__) . '/src/Entity',
        ],
        'proxy_paths' => [
            dirname(__DIR__) . '/data/doctrine-proxies',
        ],
    ],
    'view_manager' => [
        'template_path_stack' => [
            dirname(__DIR__) . '/view',
        ],
    ],
    'form_elements' => [
            'factories' => [
                'CustomInteger\Form\ConfigForm' => 'CustomInteger\Service\Form\ConfigFormFactory'
            ],
    ],
    'customInteger' => [
        'config' => [
            'foo' => '',
        ],
    ],
];