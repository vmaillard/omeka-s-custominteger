# CustomInteger (module pour Omeka S)

Module pour Omeka S qui ajoute un champ CustomInteger à tous les items sur les vues ADD, EDIT et SHOW.

Inspiré par :
- [AltText](https://github.com/zerocrates/AltText)
- [Mapping](https://github.com/omeka-s-modules/Mapping)

## Utilisation

- API resource name : "custom_integer"
- filter item search : "module:customInteger"

---

- les CustomInteger sont exposés par la Rest API :
```
api/custom_integer
```

- le CustomInteger de l'o:id voulu : 
```
api/custom_integer/{o:id_du_custom_integer}
```

- le CustomInteger lié à l'o:id voulu :
```
api/custom_integer?item_id={o:id_item}
```

- avec l'API PHP :
```
$response = $this->api()->search('custom_integer', ['item_id' => $item->id()]);
```

---

- sur l'API resource "items" : 
```
$response = $this->api()->search('items', [
    'module:customInteger' => '{integer}',
]);
```

```
api/items?module:customInteger={integer}
```


## Installation

- [Voir le manuel](http://omeka.org/s/docs/user-manual/install/)
- nom du dossier : `CustomInteger`


## Licence

GNU Affero General Public License v3.0