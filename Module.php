<?php
namespace CustomInteger;

use Doctrine\ORM\Events;
use CustomInteger\Db\Event\Listener\DetachOrphanMappings;
use Omeka\Entity\Item as ItemEntity;
use Omeka\Api\Representation\ItemRepresentation as ItemRepresentation;
use Omeka\Module\AbstractModule;
use Omeka\Permissions\Acl;
use Laminas\EventManager\Event;
use Laminas\EventManager\SharedEventManagerInterface;
use Laminas\Mvc\MvcEvent;
use Laminas\ServiceManager\ServiceLocatorInterface;


use Laminas\View\Model\ViewModel;
use Laminas\Mvc\Controller\AbstractController;
use Laminas\View\Renderer\PhpRenderer;
use CustomInteger\Form\ConfigForm;

class Module extends AbstractModule
{
    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    public function onBootstrap(MvcEvent $event)
    {
        parent::onBootstrap($event);

        $acl = $this->getServiceLocator()->get('Omeka\Acl');
        $acl->allow(
            [Acl::ROLE_AUTHOR,
                Acl::ROLE_EDITOR,
                Acl::ROLE_GLOBAL_ADMIN,
                Acl::ROLE_REVIEWER,
                Acl::ROLE_SITE_ADMIN,
            ],
            ['CustomInteger\Api\Adapter\CustomIntegerAdapter',
             'CustomInteger\Entity\CustomInteger',
            ]
        );

        $acl->allow(
            null,
            ['CustomInteger\Api\Adapter\CustomIntegerAdapter',
                'CustomInteger\Entity\CustomInteger',
            ],
            ['show', 'browse', 'read', 'search', 'create', 'update', 'delete']
            );

        $em = $this->getServiceLocator()->get('Omeka\EntityManager');
        $em->getEventManager()->addEventListener(
            Events::preFlush,
            new DetachOrphanMappings
        );
    }

    // `php application/data/scripts/update-db-data-module.php CustomInteger` pour mettre à jour le data model 
    public function install(ServiceLocatorInterface $serviceLocator)
    {
        $conn = $serviceLocator->get('Omeka\Connection');
        $conn->exec('CREATE TABLE custom_integer (id INT AUTO_INCREMENT NOT NULL, item_id INT NOT NULL, custom_integer INT DEFAULT NULL, UNIQUE INDEX UNIQ_5D56C2C6126F525E (item_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $conn->exec('ALTER TABLE custom_integer ADD CONSTRAINT FK_5D56C2C6126F525E FOREIGN KEY (item_id) REFERENCES item (id) ON DELETE CASCADE');
    }
    
    public function uninstall(ServiceLocatorInterface $serviceLocator)
    {
        $conn = $serviceLocator->get('Omeka\Connection');
        $conn->exec('DROP TABLE IF EXISTS custom_integer');
    }

    public function attachListeners(SharedEventManagerInterface $sharedEventManager)
    {
        // Affiche CustomInteger sur la vue item SHOW
        $sharedEventManager->attach(
            'Omeka\Controller\Admin\Item',
            'view.show.after',
            [$this, 'handleViewShowAfter']
        );

        // Affiche CustomInteger sur les vues item ADD et EDIT
        $sharedEventManager->attach(
            'Omeka\Controller\Admin\Item',
            'view.add.form.after',
            [$this, 'handleViewFormAfter']
        );
        $sharedEventManager->attach(
            'Omeka\Controller\Admin\Item',
            'view.edit.form.after',
            [$this, 'handleViewFormAfter']
        );

        // Add the "custom_integer" filter to item search.
        $sharedEventManager->attach(
            'Omeka\Api\Adapter\ItemAdapter',
            'api.search.query',
            [$this, 'handleApiSearchQuery']
        );

        // Ajoute CustomInteger à l'item JSON-LD
        $sharedEventManager->attach(
            'Omeka\Api\Representation\ItemRepresentation',
            'rep.resource.json',
            [$this, 'filterItemJsonLd']
        );

        // Hydrate CustomInteger pour API POST 
        $sharedEventManager->attach(
            'Omeka\Api\Adapter\ItemAdapter',
            'api.hydrate.post',
            [$this, 'hydrateCustomInteger']
        );
    }

    /*
        Affiche CustomInteger sur la vue item SHOW
    */
    public function handleViewShowAfter(Event $event)
    {
        echo $event->getTarget()->partial('common/custom-integer-view');
    }

    /*
        Affiche CustomInteger sur les vues items ADD et EDIT
    */
    public function handleViewFormAfter(Event $event)
    {
        echo $event->getTarget()->partial('common/custom-integer-form');
    }

    // Add the "custom_integer" / "module:customInteger" filter to item search.
    public function handleApiSearchQuery(Event $event)
    {
        $query = $event->getParam('request')->getContent();
        if (isset($query['module:customInteger'])) {
            $qb = $event->getParam('queryBuilder');
            $itemAdapter = $event->getTarget();
            $mappingMarkerAlias = $itemAdapter->createAlias();

            $qb->innerJoin(
                'CustomInteger\Entity\CustomInteger', 
                'cf', 
                'WITH', 
                'omeka_root' . '.id = cf.item')
                ->where('cf.customInteger = :custom_integer')
                ->setParameter('custom_integer', $query['module:customInteger']);
        }
    }

    /*
        Ajoute CustomInteger à l'item JSON-LD  
    */
    public function filterItemJsonLd(Event $event)
    {   
        $customIntegerForJsonLd = null;
        $item = $event->getTarget();
        $jsonLd = $event->getParam('jsonLd');
        $api = $this->getServiceLocator()->get('Omeka\ApiManager');

        $customInteger = $this->getCustomIntegerForItem($item);
        if ($customInteger) {
            $customIntegerForJsonLd = $customInteger->getCustomInteger();
        }
        // recup de params custom
        // $globalSettings = $this->getServiceLocator()->get('Omeka\Settings');
        // $custom_integer_setting = $globalSettings->get('custom_integer_setting');
        // $jsonLd[$custom_integer_setting] = $customIntegerForJsonLd;    
        $jsonLd['module:customInteger'] = $customIntegerForJsonLd;    
        $event->setParam('jsonLd', $jsonLd);
    }

    /*
        Renvoie CustomInteger pour un item    
    */
    public function getCustomIntegerForItem($item)
    {
        if ($item instanceof ItemRepresentation) {
            $itemId = $item->id();
        } elseif ($item instanceof ItemEntity) {
            $itemId = $item->getId();
        } else {
            throw new \InvalidArgumentException('Unexpected argument type.');
        }
    
        $entityManager = $this->getServiceLocator()->get('Omeka\EntityManager');
        $dql = 'SELECT c_integer FROM CustomInteger\Entity\CustomInteger c_integer WHERE c_integer.item = ?1';
        $query = $entityManager->createQuery($dql)->setParameter(1, $itemId);
        return $query->getOneOrNullResult();
    }

    /*
        Hydrate CustomInteger pour API POST
    */
    public function hydrateCustomInteger(Event $event)
    {
        $itemAdapter = $event->getTarget();
        $request = $event->getParam('request');

        if (!$itemAdapter->shouldHydrate($request, 'module:customInteger')) {
            return;
        }

        $item = $event->getParam('entity');
        $customInteger = $this->getCustomIntegerForItem($item);
        $requestCustomInteger = $request->getValue('module:customInteger', '');

        if (!$customInteger) {
            if ($requestCustomInteger === '') {
                return;
            }
            $customInteger = new \CustomInteger\Entity\CustomInteger;
            $customInteger->setItem($item);
            $this->getServiceLocator()->get('Omeka\EntityManager')->persist($customInteger);
        }
        
        $customInteger->setCustomInteger($requestCustomInteger);
    }


    /* USER CONFIG */

    /**
     * Get this module's configuration form.
     *
     * @param ViewModel $view
     * @return string
     */
    public function getConfigForm(PhpRenderer $renderer)
    {
        $formElementManager = $this->getServiceLocator()->get('FormElementManager');
        $form = $formElementManager->get(ConfigForm::class, []);
        return $renderer->formCollection($form, false);
    }

    /**
     * Handle this module's configuration form.
     *
     * @param AbstractController $controller
     * @return bool False if there was an error during handling
     */
    public function handleConfigForm(AbstractController $controller)
    {
        $params = $controller->params()->fromPost();
        if (isset($params['custom_integer_setting'])) {
            $custom_integer_setting = $params['custom_integer_setting'];
        }

        $globalSettings = $this->getServiceLocator()->get('Omeka\Settings');
        $globalSettings->set('custom_integer_setting', $custom_integer_setting);
    }

}

