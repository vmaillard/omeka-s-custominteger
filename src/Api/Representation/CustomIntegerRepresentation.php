<?php
namespace CustomInteger\Api\Representation;

use Omeka\Api\Representation\AbstractEntityRepresentation;

class CustomIntegerRepresentation extends AbstractEntityRepresentation
{
    public function getJsonLdType()
    {
        return 'custom_integer';
    }

    public function getJsonLd()
    {   
        // les données exposées par la REST API sur "api/custom_integer"
        return [
            'o:item' => $this->item()->getReference(),
            'module:customInteger' => $this->custom_integer(),
        ];
    }

    public function item()
    {
        return $this->getAdapter('items')
            ->getRepresentation($this->resource->getItem());
    }

    public function custom_integer()
    {
        return $this->resource->getCustomInteger();
    }
}
