<?php
namespace CustomInteger\Db\Event\Listener;

use Doctrine\ORM\Event\PreFlushEventArgs;
use CustomInteger\Entity\CustomInteger;

/**
 * Automatically detach alt texts that reference unknown items.
 */
class DetachOrphanMappings
{
    /**
     * Detach all CustomInteger entities that reference unknown items.
     *
     * @param PreFlushEventArgs $event
     */
    public function preFlush(PreFlushEventArgs $event)
    {
        $em = $event->getEntityManager();
        $uow = $em->getUnitOfWork();
        $identityMap = $uow->getIdentityMap();

        if (isset($identityMap[CustomInteger::class])) {
            foreach ($identityMap[CustomInteger::class] as $customInteger) {
                if (!$em->contains($customInteger->getItem())) {
                    $em->detach($customInteger);
                }
            }
        }
    }
}
